export PATH="/home/sathya/ti-processor-sdk-linux-am335x-evm-05.00.00.15/linux-devkit/sysroots/gcc-linaro-6.2.1-2016.11-x86_64_arm-linux-gnueabihf/bin:$PATH"

//***********Kernel Compillation***********

make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- distclean
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- atop_am335x_defconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig  (if required to change any parameter, not required as of now)
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- uImage LOADADDR=0x82000000
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- am335x-boneblack-atop.dtb
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- modules  (if required, not required as of now)

//***********TFTP Loading***************:
setenv ipaddr 192.168.1.193
setenv serverip 192.168.1.10
setenv netmask 255.255.255.0
setenv gatewayip 192.168.1.1

protect off all
erase 1:8-51

tftp sathya/uImage
cp.b 0x82000000 0x08120000 uImage_size

tftp 0x85000000 ramdisk_ethtools.gz [Copied in the git, needed to flash the jffs2 generated from build root]

tftp 0x84000000 sathya/am335x-boneblack-atop.dtb
cp.b 0x84000000 0x080e0000 dtb_size

tftp 0x85000000 file-system.jffs2


setenv bootargs 'root=/dev/ram0 rw initrd=0x85000000,8M console=ttyS0,115200 n8 mem=256M'
bootm 0x82000000 - 0x84000000


setenv ipaddr 192.168.1.193
setenv serverip 192.168.1.95
setenv netmask 255.255.255.0
setenv gateway 192.168.1.1
saveenv

//***************SPI Flashing**************************
tftp 0x82000000 MLO.byteswap
tftp 0x82000000 u-boot.img
tftp uImage
tftp 0x84000000 am335x-boneblack-atop.dtb
tftp 0x85000000 ramdisk_ethtools.gz
tftp 0x85000000 root-jffs2.bin

//MLO w25q128fw
sf probe 0
sf erase 0 0x120000        				
sf write  0x82000000 0 	{sizeof_image}     MLO.byteswap
sf write  0x82000000 0x20000 {sizeof_image}   	//Uboot

sf erase 0x120000 0x400000         		//uImage
sf write  0x82000000 0x120000 {sizeof_image}



sf erase 0x520000 0x10000         		//am335x-bone.dtb
sf write  0x84000000 0x520000   	{sizeof_image}
sf erase 0x530000 0x400000 			//ramdisk_ethtools
sf write  0x85000000 0x530000 {sizeof_image}



/*******SPI-Boot Command*********/

setenv bootcmd 'sf probe 0;sf read 0x82000000 0x120000 0x400000;sf read 0x84000000 0x520000 0x10000;bootm 0x82000000 - 0x84000000'
setenv bootargs 'root=/dev/mtdblock6 rw rootfstype=jffs2, console=ttyS0,115200 n8 mem=256M'
setenv ipaddr 192.168.1.193
setenv serverip 192.168.1.95
setenv netmask 255.255.255.0
setenv gateway 192.168.1.1
saveenv






