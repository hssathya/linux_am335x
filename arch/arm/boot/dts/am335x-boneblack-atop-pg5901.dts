/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;

#include "am33xx.dtsi"
#include "am335x-bone-common.dtsi" 
#include <dt-bindings/display/tda998x.h>

/ {
	model = "TI AM335x BeagleBone Black";
	compatible = "ti,am335x-bone-black", "ti,am335x-bone", "ti,am33xx";
};

&ldo3_reg {
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;
	regulator-always-on;
};

&mmc1 {
	vmmc-supply = <&vmmcsd_fixed>;
};

&cpu0_opp_table {
	/*
	 * All PG 2.0 silicon may not support 1GHz but some of the early
	 * BeagleBone Blacks have PG 2.0 silicon which is guaranteed
	 * to support 1GHz OPP so enable it for PG 2.0 on this board.
	 */
	oppnitro@1000000000 {
		opp-supported-hw = <0x06 0x0100>;
	};
};

&am33xx_pinmux {
	gpmc_pins: gpmc_pins{
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x8a4, PIN_OUTPUT | MUX_MODE1)		/* lcd_data1.lcd_data1 */ /* NOR_A1 */
			AM33XX_IOPAD(0x8a8, PIN_OUTPUT | MUX_MODE1)		/* lcd_data2.lcd_data2 */ /* NOR_A2 */
			AM33XX_IOPAD(0x8ac, PIN_OUTPUT | MUX_MODE1)		/* lcd_data3.lcd_data3 */ /* NOR_A3 */
			AM33XX_IOPAD(0x8b0, PIN_OUTPUT | MUX_MODE1)		/* lcd_data4.lcd_data4 */ /* NOR_A4 */
			AM33XX_IOPAD(0x8b4, PIN_OUTPUT | MUX_MODE1)		/* lcd_data5.lcd_data5 */ /* NOR_A5 */
			AM33XX_IOPAD(0x8b8, PIN_OUTPUT | MUX_MODE1)		/* lcd_data6.lcd_data6 */ /* NOR_A6 */
			AM33XX_IOPAD(0x8bc, PIN_OUTPUT | MUX_MODE1)		/* lcd_data7.lcd_data7 */ /* NOR_A7 */
			AM33XX_IOPAD(0x8e0, PIN_OUTPUT | MUX_MODE1)   	/* lcd_vsync.lcd_vsync */
			AM33XX_IOPAD(0x8e4, PIN_OUTPUT | MUX_MODE1)	    /* lcd_hsync.lcd_hsync */
			
			AM33XX_IOPAD(0x800, INPUT_EN | MUX_MODE0)	/* gpmc_ad0.gpmc_ad0 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x804, INPUT_EN | MUX_MODE0)	/* gpmc_ad1.gpmc_ad1 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x808, INPUT_EN | MUX_MODE0)	/* gpmc_ad2.gpmc_ad2 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x80c, INPUT_EN | MUX_MODE0)	/* gpmc_ad3.gpmc_ad3 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x810, INPUT_EN | MUX_MODE0)	/* gpmc_ad4.gpmc_ad4 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x814, INPUT_EN | MUX_MODE0)	/* gpmc_ad5.gpmc_ad5 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x818, INPUT_EN | MUX_MODE0)	/* gpmc_ad6.gpmc_ad6 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x81c, INPUT_EN | MUX_MODE0)	/* gpmc_ad7.gpmc_ad7 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x820, INPUT_EN | MUX_MODE0)	/* gpmc_ad8.gpmc_ad8 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x824, INPUT_EN | MUX_MODE0)	/* gpmc_ad9.gpmc_ad9 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x828, INPUT_EN | MUX_MODE0)	/* gpmc_ad10.gpmc_ad10 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x82c, INPUT_EN | MUX_MODE0)	/* gpmc_ad11.gpmc_ad11 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x830, INPUT_EN | MUX_MODE0)	/* gpmc_ad12.gpmc_ad12 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x834, INPUT_EN | MUX_MODE0)	/* gpmc_ad13.gpmc_ad13 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x838, INPUT_EN | MUX_MODE0)	/* gpmc_ad14.gpmc_ad14 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x83c, INPUT_EN | MUX_MODE0)	/* gpmc_ad15.gpmc_ad15 */ /* NOR_AD0 */
			AM33XX_IOPAD(0x87c, PIN_OFF_INPUT_PULLUP | SLEWCTRL_FAST | MUX_MODE0)	/* gpmc_csn0.gpmc_csn0 */
 			AM33XX_IOPAD(0x888, PIN_OFF_INPUT_PULLUP | SLEWCTRL_FAST | MUX_MODE0)	/* gpmc_csn3.gpmc_csn3 */
			AM33XX_IOPAD(0x890, PIN_OFF_INPUT_PULLUP | MUX_MODE0)		/* gpmc_advn_ale.gpmc_advn_ale */
			AM33XX_IOPAD(0x894, PIN_OFF_INPUT_PULLUP | MUX_MODE0)		/* gpmc_oen_ren.gpmc_oen_ren */
			AM33XX_IOPAD(0x898, PIN_OFF_INPUT_PULLUP | MUX_MODE0)		/* gpmc_wen.gpmc_wen */

			AM33XX_IOPAD(0x878, PIN_OFF_INPUT_PULLUP | MUX_MODE4)		/* gpmc_be1n */
			
			
			AM33XX_IOPAD(0x9e8, SLEWCTRL_FAST | MUX_MODE7)	/* emu1.gpio3[8] */  /* OEn_REN */
			AM33XX_IOPAD(0x868, INPUT_EN | PULL_DISABLE | MUX_MODE7)  /* gpio1[26] gpmc.a10 */	
			AM33XX_IOPAD(0x86c, INPUT_EN | PULL_DISABLE | MUX_MODE7) /*gpio1[27] gpmc.a11 */
			AM33XX_IOPAD(0x9a8, INPUT_EN | PULL_DISABLE | MUX_MODE7) /*gpio3[20]  mcasp0_ax1 */
 			AM33XX_IOPAD(0x870, INPUT_EN | PULL_DISABLE | MUX_MODE7) /*gpio0[30] */ 
 			>;
	};
	spi0_pins: pinmux_spi0_pins {
		pinctrl-single,pins = <
			/* spi0_sclk.spi0_sclk */
			AM33XX_IOPAD(0x950,  INPUT_EN | MUX_MODE0)
			/* spi0_d0.spi0_d0 */
			AM33XX_IOPAD(0x954,  INPUT_EN | MUX_MODE0)
			/* spi0_d1.spi0_d1 */
			AM33XX_IOPAD(0x958,  INPUT_EN | MUX_MODE0)
			/* spi0_cs0.spi0_cs0 */
			AM33XX_IOPAD(0x95C,  INPUT_EN | MUX_MODE0)
			>;
	};

	uart1_pins: pinmux_uart1_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x980, PIN_INPUT_PULLUP | MUX_MODE0)	/* uart1.rxd */  
			AM33XX_IOPAD(0x984, PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* uart1_txd */
			AM33XX_IOPAD(0x97c, PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* uart1_rts_n */
			>;
	};
	

	uart2_pins: pinmux_uart2_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x900, INPUT_EN | MUX_MODE3)	/* uart2.rxd */  /* mmc0.clk */
			AM33XX_IOPAD(0x904, INPUT_EN | MUX_MODE3)	/* uart2_txd */  /* mmc0.cmd */
		>;
	};

	nxp_hdmi_bonelt_off_pins: nxp_hdmi_bonelt_off_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x9b0, PIN_OUTPUT_PULLDOWN | MUX_MODE3)	/* xdma_event_intr0 */
		>;
	};

	
};
&rtc {
	system-power-controller;
};



&sgx {
	status = "okay";
};
/*
/ { 
	clk_mcasp0_fixed: clk_mcasp0_fixed {
		#clock-cells = <0>;
		compatible = "fixed-clock";
		clock-frequency = <24576000>;
	};

	clk_mcasp0: clk_mcasp0 {
		#clock-cells = <0>;
		compatible = "gpio-gate-clock";
		clocks = <&clk_mcasp0_fixed>;
		enable-gpios = <&gpio1 27 0>; /* BeagleBone Black Clk enable on GPIO1_27 
	};
}; */

&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart1_pins>;

	status = "okay";
};



&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart2_pins>;

	status = "okay";
};


&spi0 {
	#address-cells = <1>;
	#size-cells = <0>;

	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&spi0_pins>;
	spidev@0 {
                 spi-max-frequency = <24000000>;
                 reg = <0>;
                 compatible = "spidev";
            };
};
&gpmc {
	compatible = "ti,am3352-gpmc";
	ti,hwmods = "gpmc";
	status = "okay";
	gpmc,num-waitpins = <2>;
	pinctrl-names = "default";
	pinctrl-0 = <&gpmc_pins>;

	#address-cells = <2>;
	#size-cells = <1>;
	ranges = <	0 0 0x08000000 0x02000000		/* NOR 32M */
			3 0 0x0A000000 0x01000000	>;	
	nor@0,0 {
		reg = <0 0x00000000 0x2000000>;
		compatible = "cfi-flash";
		linux,mtd-name = "spansion,s29gl256p11t";
		bank-width = <2>;

		gpmc,mux-add-data = <2>;
		gpmc,device-width = <2>;
		gpmc,sync-clk-ps = <0>;
		gpmc,cs-on-ns = <0>;
		gpmc,cs-rd-off-ns = <160>;
		gpmc,cs-wr-off-ns = <160>;
		gpmc,adv-on-ns = <10>;
		gpmc,adv-rd-off-ns = <30>;
		gpmc,adv-wr-off-ns = <30>;
		gpmc,oe-on-ns = <40>;
		gpmc,oe-off-ns = <160>;
		gpmc,we-on-ns = <40>;
		gpmc,we-off-ns = <160>;
		gpmc,rd-cycle-ns = <160>;
		gpmc,wr-cycle-ns = <160>;
		gpmc,access-ns = <150>;
		gpmc,page-burst-access-ns = <10>;
		gpmc,cycle2cycle-samecsen;
		gpmc,cycle2cycle-delay-ns = <20>;
		gpmc,wr-data-mux-bus-ns = <70>;
		gpmc,wr-access-ns = <80>;
	

		#address-cells = <1>;
		#size-cells = <1>;
       partition@0 {
            label = "u-boot";
            reg = <0x00000000 0x000A0000>;  /* 640KB */
        };
        partition@1 {
            label = "u-boot env1";
            reg = <0x000A0000 0x00020000>;  /*128KB */
        };
	partition@2 {
            label = "u-boot env2";
            reg = <0x000C0000 0x00020000>;
        };
        partition@3 {
            label = "DTB";
            reg = <0x000E0000 0x00040000>;
        };
        partition@4 {
            label = "kernel";
            reg = <0x00120000 0x00500000>;
        };
        partition@5 {
            label = "rootfs";
            reg = <0x00600000 0x019E0000>;
        };
};
	dpram@3,0 {
		compatible = "atop-dpram";                 
		#address-cells = <1>;
                #size-cells = <1>;
                reg = <3 0x000000 0x1000>;    /* CS3, offset 0,  */
 		bank-width = <2>;
		device-width = <2>;
		gpmc,sync-clk-ps = <20000>;
		gpmc,sync-read = "true"; 
   		gpmc,sync-write = "true";
                gpmc,mux-add-data = <2>;
                gpmc,cs-on-ns = <0>;
                gpmc,cs-rd-off-ns = <200>;/*400*/
                gpmc,cs-wr-off-ns = <200>; /*400*/
                gpmc,adv-on-ns = <0>;
                gpmc,adv-rd-off-ns = <40>;
                gpmc,adv-wr-off-ns = <40>;
                gpmc,oe-on-ns = <40>;
                gpmc,oe-off-ns = <360>;/*400*/
                gpmc,we-on-ns = <40>;
                gpmc,we-off-ns = <200>;
                gpmc,rd-cycle-ns = <360>;/*360*/
                gpmc,wr-cycle-ns = <360>;/*360*/
                gpmc,access-ns = <320>;/*320*/
                gpmc,wr-data-mux-bus-ns = <40>;             
	};
};

